require 'addressable/uri'
require 'rest-client'

def index_contact(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}/contacts.html"
  ).to_s

  puts RestClient.get(url)
end

index_contact(1)