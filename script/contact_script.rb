require 'addressable/uri'
require 'rest-client'

def index_contact
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/contacts.html"
    ).to_s

    puts RestClient.get(url)
end


def show_contact(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/contacts/#{id}.html"
    ).to_s

  puts RestClient.get(url)
end

def create_contact(name, email, id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: '/contacts.html'
  ).to_s

  puts RestClient.post(
    url, { :contact => { :name => name, :email => email, :user_id => id } } )
end

def update_contact(name, email, id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/contacts/#{id}.html"
  ).to_s

  puts RestClient.put(url, { :contact => { :name => name, :email => email, :user_id = id } } )
end

def destroy_contact(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/contacts/#{id}.html"
  ).to_s

  puts RestClient.delete(url)
end

destroy_contact(2)