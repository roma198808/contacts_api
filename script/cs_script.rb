require 'addressable/uri'
require 'rest-client'

def create_share(user_id, contact_id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: '/contact_shares.html'
  ).to_s

  puts RestClient.post(
    url, { :contact_share => { :user_id => user_id, :contact_id => contact_id }})
end

def destroy_share(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/contact_shares/#{id}.html"
  ).to_s

  puts RestClient.delete(url)
end

destroy_share(4)