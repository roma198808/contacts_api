require 'addressable/uri'
require 'rest-client'


def show_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
    # query_values: {
  #     'categorya[key]' => 'a_value',
  #     'categoryb[keyb]' => 'b_value',
  #     'categoryc[inner_hash_key][key]' => 'inner_value'
  #   }
  ).to_s

  puts RestClient.get(url)
end

def create_user
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: '/users.html'
  ).to_s

  puts RestClient.post(
    url,
    { :user => { :username => "bob" }
  )
end

def update_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
  ).to_s

  puts RestClient.put(url, { :user => { :username => "User6" } })
end

def destroy_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
  ).to_s

  RestClient.delete(url)
end