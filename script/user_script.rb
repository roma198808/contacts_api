require 'addressable/uri'
require 'rest-client'

def index_user
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users.html"
    ).to_s

    puts RestClient.get(url)
end


def show_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
    ).to_s

  puts RestClient.get(url)
end

def create_user(name)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: '/users.html'
  ).to_s

  puts RestClient.post(
    url, { :user => { :username => name } } )
end

def update_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
  ).to_s

  puts RestClient.put(url, { :user => { :username => "User4" } })
end

def destroy_user(id)
  url = Addressable::URI.new(
    scheme: 'http',
    host: 'localhost',
    port: 3000,
    path: "/users/#{id}.html"
  ).to_s

  puts RestClient.delete(url)
end

index_user