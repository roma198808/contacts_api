class User < ActiveRecord::Base
  attr_accessible :username

  attr_reader :id

  validates :username, :presence => true, :uniqueness => true

  has_many :shared_contacts, :class_name => "ContactShare", :foreign_key => :user_id
  has_many :contacts, :class_name => "Contact", :foreign_key => :user_id
end
