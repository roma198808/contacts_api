class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :user_id, presence: true

  has_many :shared_users, :class_name => "ContactShare", :foreign_key => :contact_id
  belongs_to :owner, :class_name => "User", :foreign_key => :user_id

  def self.contacts_for_user_id(user_id)
    join = <<-SQL
           LEFT OUTER JOIN contact_shares ON contacts.id = contact_shares.contact_id
              SQL

    where_stuff = <<-SQL
                   contacts.user_id = #{user_id} OR contact_shares.user_id = #{user_id}
                     SQL

    Contact.joins(join).where(where_stuff)
  end

end
