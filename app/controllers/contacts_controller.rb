class ContactsController < ApplicationController

  def index
    # user = params[:user_id]
    user_contacts = Contact.contacts_for_user_id(params[:user_id])
    render :json => user_contacts
  end

  def create
    # yell unless user_id = Contact.new(params[:contact][:user_id])
    contact = Contact.new(params[:contact])
    if contact.save
      render :json => contact
    else
      render :json => contact.errors.full_messages, :status => :unprocessable_entity
    end
  end

  def show
    contact = Contact.find(params[:id])
    render :json => contact
  end

  def update
    contact = Contact.find(params[:id])
    contact.update_attributes(params[:contact])
    render :json => contact
  end

  def destroy
    Contact.find(params[:id]).destroy

    render :text => "Contact destroyed"
  end

end

