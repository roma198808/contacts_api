class ContactSharesController < ApplicationController
  def create
    new_share = ContactShare.new(params[:contact_share])
    if new_share.save
      render :json => new_share
    else
      render :json => new_share.errors.full_messages, :status => :unprocessable_entity
    end
  end

  def destroy
    share = ContactShare.find(params[:id]).destroy
    render :text => "Connection has been destroyed: #{share}"
  end
end
