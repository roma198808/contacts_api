class AddIndicesToContactShares < ActiveRecord::Migration
  def change
    add_index :contact_shares, [:contact_id, :user_id], :uniqueness => true
  end
end
