# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!([{username: "Username1"},
              {username: "Username2"},
              {username: "Username3"},
              {username: "Username4"},
              {username: "Username5"}])

Contact.create!([{name: "Name2", email: "Email2", user_id: 1},
                 {name: "Name3", email: "Email3", user_id: 1},
                 {name: "Name4", email: "Email4", user_id: 1},
                 {name: "Name5", email: "Email5", user_id: 2}])

ContactShare.create!({user_id: 1, contact_id: 4})